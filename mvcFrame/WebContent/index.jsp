<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp" %>
<%@ include file="/WEB-INF/views/include/common.jsp" %>
<html>
<head>
	<title>jQuery Easyui v1.4.1 Demo</title>
	<script type="text/javascript">
		$(function(){
			//为顶部导航栏菜单添加单击事件
			$('#navMenu a').click(function() {
				$('#navMenu a').removeClass('active');
				$(this).addClass('active');
				
				clearNav();
				var menuName = $(this).attr('name');
				initLeftMenu(menuName);
			});
			//初始化基础数据的菜单
			$('#basic').click();
			//去掉accord的动画，有一些bug
			$("#accMenu").accordion( {
				animate : false
			});
		});
		//初始化菜单方法
		function initLeftMenu(navName){
			//初始化菜单
			$.ajax({  
				url:"menu.json",
				data:{"navName":navName},
				dataType:"json",
				success: function(data){
					//获取head导航菜单中的名字
					addNav(data[navName]);
					initMenu();
				}
			});
		}
	</script>
	<style>
		#navMenu li{ float:left; list-style-type:none;}
		#navMenu li a{	color:#fff; padding-right:20px;}
		#navMenu li a.active{color:yellow;}
	</style>
</head>
<body class="easyui-layout" style="overflow-y: hidden"  scroll="no">
	<noscript>
		<div
			style="position: absolute; z-index: 100000; height: 2046px; top: 0px; left: 0px; width: 100%; background: white; text-align: center;">
			<img src="images/noscript.gif" alt='抱歉，请开启脚本支持！' />
		</div>
	</noscript>
	<div region="north" split="true" border="false"
		style="overflow: hidden; height: 30px; background: url(${ctxStatic}/images/layout-browser-hd-bg.gif) #7f99be repeat-x center 50%; line-height: 20px; color: #fff; font-family: Verdana, 微软雅黑, 黑体">
		<span style="float: right; padding-right: 20px;" class="head">
			欢迎 admin <a href="#" id="editpass">修改密码</a> 
			<a href="#" id="loginOut">安全退出</a>
		</span> 
		<span style="padding-left: 10px; font-size: 16px; float: left;">
			<img src="${ctxStatic}/images/blocks.gif" width="20" height="20" align="absmiddle" /> 项目框架
		</span>
		<ul id="navMenu"
			style="padding: 0px; margin: 0px; list-type: none; float: left; margin-left: 40px;">
			<li>
				<a id="basic" class="active" name="basic" href="javascript:;" title="基础数据">基础数据</a>
			</li>
			<li>
				<a id="point" name="point" href="javascript:;" title="积分管理">积分管理</a>
			</li>
		</ul>
	</div>
	<div region="south" split="true" style="height: 30px; background: #D2E0F2;">
		<div class="footer">Copyright © 2012-2015 普信恒业科技发展（北京）有限公司</div>
	</div>
	<div id="west" region="west" hide="true" split="true" title="导航菜单" style="width: 180px;">
		<div id='accMenu' class="easyui-accordion" fit="true" border="false">
			<!--  导航内容 -->
		</div>
	</div>
	<div id="mainPanle" region="center"
		style="background: #eee; overflow-y: hidden">
		<div id="tabs" class="easyui-tabs" fit="true" border="false">
			<div title="首页" style="padding: 20px; overflow: hidden;" id="home">
				<h1>Welcome to using The jQuery EasyUI!</h1>
			</div>
		</div>
	</div>

	<div id="mm" class="easyui-menu" style="width: 150px;">
		<div id="mm-tabupdate">刷新</div>
		<div class="menu-sep"></div>
		<div id="mm-tabclose">关闭</div>
		<div id="mm-tabcloseall">全部关闭</div>
		<div id="mm-tabcloseother">除此之外全部关闭</div>
		<div class="menu-sep"></div>
		<div id="mm-tabcloseright">当前页右侧全部关闭</div>
		<div id="mm-tabcloseleft">当前页左侧全部关闭</div>
		<div class="menu-sep"></div>
		<div id="mm-exit">退出</div>
	</div>
</body>
</html>