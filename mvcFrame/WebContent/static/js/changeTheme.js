/* 更换主题 */
function changeThemeFun(themeName) {
	var $easyuiTheme = $('#easyuiTheme');
	var url = $easyuiTheme.attr('href');
	var href = url.substring(0, url.indexOf('themes')) + 'themes/' + themeName + '/easyui.css';
	$('#easyuiTheme').attr('href', href);
	
	/*如果项目使用了iframe，那么把iframe里的每个主题都切换*/
//	var $iframe = $('iframe');
//	if ($iframe.length > 0) {
//		for ( var i = 0; i < $iframe.length; i++) {
//			var ifr = $iframe[i];
//			$(ifr).contents().find('#easyuiTheme').attr('href', href);
//		}
//	}
	//设置cookie值，有限期7天
	$.cookie('easyuiThemeName', themeName, {
		expires : 7
	});
};
window.onload = function () {
	//如果cookie中有值，切换主题
	var skin = $.cookie('easyuiThemeName');
	if (skin) {
		changeThemeFun(skin);
        $('#themes').combobox('setValue', skin);
	}
};