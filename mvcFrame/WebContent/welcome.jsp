<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp" %>
<%@ include file="/WEB-INF/views/include/common.jsp" %>
<html>
<head>
	<title>jQuery Easyui v1.4.1 Demo</title>
	<style type="text/css">
		.logo {
	        font-family:"微软雅黑",	"Helvetica Neue",​Helvetica,​Arial,​sans-serif;
	        font-size:28px;font-weight:bold;color:#444;        
	        cursor:default;
	        position:absolute;top:28px;left:15px;        
	        line-height:28px;
	    }
	    .topNav
	    {
	        position:absolute;right:8px;top:10px;        
	        font-size:12px;
	        line-height:25px;
	    }
	    .topNav a
	    {
	        text-decoration:none;
	        color:#222;
	        font-weight:normal;
	        font-size:12px;
	        line-height:25px;
	        margin-left:3px;
	        margin-right:3px;
	    }
	    .topNav a:hover
	    {
	        text-decoration:underline;
	        color:Blue;
	    }
	</style>
	<script type="text/javascript">
		$(function() {
			$('#themes').combobox({
				panelHeight : 100,
				onChange : function(themeName, preTheme) {
					changeThemeFun(themeName);
				}
			});
		});
	</script>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',split:true" style="height:80px">
		<div class="logo">mvc项目框架 示例</div>

        <div class="topNav">    
            <a href="#">首页</a> |
            <a href="#">在线示例</a> |
            <a href="#">Api手册</a> |            
            <a href="#">开发教程</a> |
            <a href="#">快速入门</a>
        </div>

        <div style="position:absolute;right:12px;bottom:5px;font-size:12px;line-height:25px;font-weight:normal;">
            <span style="color:Red;font-family:Tahoma">（推荐Blue）</span>选择皮肤：
            <select id="themes" onchange="onSkinChange(this.value)" style="width:100px;" >
                <option value="default">default</option>
				<option value="black">black</option>
				<option value="gray">gray</option>
				<option value="bootstrap">bootstrap</option>
				<option value="metro">metro</option>
            </select>
        </div>
	</div>
	<div data-options="region:'south',split:true" style="border:0;text-align:center;height: 30px;background: #D2E0F2;">
		<div class="footer">Copyright © 2012-2015 普信恒业科技发展（北京）有限公司</div>
	</div>
	
	<!-- <div data-options="region:'east',split:true" title="East" style="width:100px;"></div> -->
	<div id="west" data-options="region:'west',split:true" title="导航菜单" style="width:200px;">
		<div id='wnav' class="easyui-accordion" fit="true" border="false">
			<!--  导航内容 -->

		</div>
	</div>
	
	<div id="mainPanle" region="center" style="background: #eee; overflow-y: hidden">
		<div id="tabs" class="easyui-tabs" fit="true" border="false">
			<div title="欢迎使用" style="padding: 20px; overflow: hidden;" id="home">
				<h1>Welcome to using The jQuery EasyUI!</h1>
			</div>
		</div>
	</div>
	
	<!-- 选项卡右键菜单 -->
	<div id="mm" class="easyui-menu" style="width:150px;display:none">
		<div id="mm-tabupdate">刷新</div>
		<div class="menu-sep"></div>
		<div id="mm-tabclose">关闭</div>
		<div id="mm-tabcloseall">全部关闭</div>
		<div id="mm-tabcloseother">除此之外全部关闭</div>
		<div class="menu-sep"></div>
		<div id="mm-tabcloseleft">当前页左侧全部关闭</div>
		<div id="mm-tabcloseright">当前页右侧全部关闭</div>
	</div>
</body>
</html>