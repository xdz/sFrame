<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-store" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="X-UA-Compatible" content="IE=7,IE=8,IE=9,IE=10" />
<link id="easyuiTheme" rel="stylesheet" type="text/css" href="${ctxStatic}/jquery-easyui-1.4.1/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${ctxStatic}/jquery-easyui-1.4.1/themes/icon.css"/>
<script type="text/javascript" src="${ctxStatic}/jquery-easyui-1.4.1/jquery.min.js"></script>
<!-- cookie.js和changeEasyuiThee.js主要用于切换主题 -->
<script type="text/javascript" src="${ctxStatic}/jquery-plugins/jquery.cookie.js"></script>
<script type="text/javascript" src="${ctxStatic}/js/changeTheme.js"></script>
<script type="text/javascript" src="${ctxStatic}/jquery-easyui-1.4.1/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${ctxStatic}/jquery-easyui-1.4.1/locale/easyui-lang-zh_CN.js"></script>

<link rel="stylesheet" type="text/css" href="${ctxStatic}/css/default.css" />
<script type="text/javascript" src="${ctxStatic}/js/outlook.js"></script>