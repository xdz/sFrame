/* 
 * Copyright (C) 2006-2015 普信恒业科技发展（北京）有限公司.
 * 本系统是商用软件,未经授权擅自复制或传播本程序的部分或全部将是非法的.
============================================================
 * FileName: CodeRuleServiceImplTest.java 
 * Created: [2015年4月17日 下午6:48:12] by  
 * $Id$: 
 * $Revision$: v1.0
 * $Author$: chenshuai
 * $Date$
============================================================ 
 * ProjectName: mvcFrame 
 * Description: 
==========================================================*/

package com.creditease.modules.coderule.service;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.creditease.sframe.junit.SpringJunitSupport;

/** 
 * @Description: 
 * @Author: chenshuai
 * @Company: 普恒-基础框架
 * @Version: V1.0
 * @Create Date: 2015年4月17日
 */
public class CodeRuleServiceImplTest extends SpringJunitSupport {

	@Autowired
	private ICodeRuleService codeRuleService;
	
	@Test
	public void testGetBizCode() {
		String matchCode = codeRuleService.getBizCode("MN");
		System.out.println(matchCode);
		Assert.assertNotNull(matchCode);
	}

}
