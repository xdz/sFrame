/* 
 * Copyright (C) 2006-2015 普信恒业科技发展（北京）有限公司.
 * 本系统是商用软件,未经授权擅自复制或传播本程序的部分或全部将是非法的.
============================================================
 * FileName: JUnit4Support.java 
 * Created: [2015年3月16日 上午7:47:07] by  
 * $Id$: 
 * $Revision$: v1.0
 * $Author$: chenshuai
 * $Date$
============================================================ 
 * ProjectName: mvcFrame 
 * Description: 
==========================================================*/

package com.creditease.sframe.junit.sample;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.creditease.sframe.junit.Junit4Support;

/** 
 * @Description: JUnit4通过注解的方式来识别测试方法。目前支持的主要注解有：
	--@BeforeClass 全局只会执行一次，而且是第一个运行
	--@Before 在测试方法运行之前运行
	--@Test 测试方法
	--@After 在测试方法运行之后允许
	--@AfterClass 全局只会执行一次，而且是最后一个运行
	--@Ignore 忽略此方法
 * @Author: chenshuai
 * @Company: 普恒-基础框架
 * @Version: V1.0
 * @Create Date: 2015年3月16日
 */
public class Junit4TestCase extends Junit4Support{  
   
    @Test  
    public void testMathPow() {  
        System.out.println("Test Math.pow");  
        Assert.assertEquals(4.0, Math.pow(2.0, 2.0), 0.0);  
    }  
   
    @Test  
    public void testMathMin() {  
        System.out.println("Test Math.min");  
        Assert.assertEquals(2.0, Math.min(2.0, 4.0), 0.0);  
    }  
   
    // 期望此方法抛出NullPointerException异常  
    @Test(expected = NullPointerException.class)  
    public void testException() {  
        System.out.println("Test exception");  
        Object obj = null;  
        obj.toString();  
    }  
   
    // 忽略此测试方法  
    @Ignore  
    @Test  
    public void testMathMax() {  
        Assert.fail("没有实现");  
    }  
    // 使用“假设”来忽略测试方法  
    @Test  
    public void testAssume(){  
        System.out.println("Test assume");  
        // 当假设失败时，则会停止运行，但这并不会意味测试方法失败。  
        Assume.assumeTrue(false);  
        Assert.fail("没有实现");  
    }  
} 