/* 
 * Copyright (C) 2006-2015 普信恒业科技发展（北京）有限公司.
 * 本系统是商用软件,未经授权擅自复制或传播本程序的部分或全部将是非法的.
============================================================
 * FileName: SpringJUnitSupport.java 
 * Created: [2015年3月16日 上午7:42:50] by  
 * $Id$: 
 * $Revision$: v1.0
 * $Author$: chenshuai
 * $Date$
============================================================ 
 * ProjectName: mvcFrame 
 * Description: 
==========================================================*/

package com.creditease.sframe.junit;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/** 
 * @Description: 单元测试的基础类，基础SpringJUnit的方式，其他测试类继承此类即可
 *  --@BeforeClass 全局只会执行一次，而且是第一个运行
 *	--@Before 在测试方法运行之前运行
 *	--@Test 测试方法
 *	--@After 在测试方法运行之后允许
 *	--@AfterClass 全局只会执行一次，而且是最后一个运行
 *	--@Ignore 忽略此方法
 * @Author: chenshuai
 * @Company: 普恒-基础框架
 * @Version: V1.0
 * @Create Date: 2015年3月16日
 */
@RunWith(SpringJUnit4ClassRunner.class)
// 指定测试用例的运行器 这里是指定了Junit4
@ContextConfiguration({ "classpath:spring/applicationContext.xml",
		"classpath:mybatis/applicationContext-mybatis.xml" })
// 指定Spring的配置文件 /为classpath下
// @Transactional //对所有的测试方法都使用事务，并在测试完成后回滚事务
public class SpringJunitSupport {

}
