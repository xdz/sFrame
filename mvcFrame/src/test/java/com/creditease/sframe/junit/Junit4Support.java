/* 
 * Copyright (C) 2006-2015 普信恒业科技发展（北京）有限公司.
 * 本系统是商用软件,未经授权擅自复制或传播本程序的部分或全部将是非法的.
============================================================
 * FileName: JUnit4Support.java 
 * Created: [2015年3月16日 上午7:47:07] by  
 * $Id$: 
 * $Revision$: v1.0
 * $Author$: chenshuai
 * $Date$
============================================================ 
 * ProjectName: mvcFrame 
 * Description: 
==========================================================*/

package com.creditease.sframe.junit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/** 
 * @Description: JUnit4通过注解的方式来识别测试方法。目前支持的主要注解有：
	--@BeforeClass 全局只会执行一次，而且是第一个运行
	--@Before 在测试方法运行之前运行
	--@Test 测试方法
	--@After 在测试方法运行之后允许
	--@AfterClass 全局只会执行一次，而且是最后一个运行
	--@Ignore 忽略此方法
 * @Author: chenshuai
 * @Company: 普恒-基础框架
 * @Version: V1.0
 * @Create Date: 2015年3月16日
 */
public class Junit4Support {  
   
    @BeforeClass  
    public static void setUpBeforeClass() {  
        //全局只会执行一次，而且是第一个运行
    	//加载spring配置
    	ApplicationContext context = new ClassPathXmlApplicationContext(new String[] {
				"classpath:/spring-servlet.xml",
				"classpath:/spring/**/*.xml"
//				"classpath:/config/log/logAdvice.xml"
				});
    }  
   
    @Before  
    public void setUp() throws Exception {  
        System.out.println("Set up");  
    }  
   
    @After  
    public void tearDown() throws Exception {  
        System.out.println("Tear down");  
    }  
   
    @AfterClass  
    public static void tearDownAfterClass() {  
        System.out.println("Tear down After class");  
    }  
} 