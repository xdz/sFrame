一、重复提交处理
/**
 * <p>
 * 防止重复提交注解，用于方法上<br/>
 * 在新建页面方法上，设置saveToken()为true，此时拦截器会在Session中保存一个token，
 * 同时需要在新建的页面中添加
 * <input type="hidden" name="token" value="${token}">
 * <br/>
 * 保存方法需要验证重复提交的，设置removeToken为true
 * 此时会在拦截器中验证是否重复提交
 * </p>
 * @author: chenshuai
 * @date: 2015-01-21上午18:51:02
 *
 */
1.新建注解：见Token.java
2.新建拦截器：见TokenInterceptor.java
3.在Spring中配置
 <!-- 拦截器配置 -->
<mvc:interceptors>
	<!-- 配置Token拦截器，防止用户重复提交数据 -->
	<mvc:interceptor>
		<mvc:mapping path="/**"/>
	    <bean class="com.creditease.sframe.interceptor.TokenInterceptor"/>
	</mvc:interceptor>
</mvc:interceptors>
4.在相关方法中加入注解
@RequestMapping("/save")
@Token(removeToken=true)
public synchronized ModelAndView save(HttpServletRequest request, HttpServletResponse response)throws Exception {
 
@RequestMapping("/edit")
@Token(saveToken=true)
public ModelAndView edit(HttpServletRequest request) throws Exception {
5.在新建页面中加入
  	<input type="hidden" name="token" value="${token}">