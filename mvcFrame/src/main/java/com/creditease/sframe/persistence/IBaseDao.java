package com.creditease.sframe.persistence;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
/**
 * 数据库操作的基础接口，各模块所有的DAO都需要继承此接口
 * @author chenshuai
 * @date 2015-01-23 20:02:05
 * @param <T>
 */
public interface IBaseDao<T> {

	/**
	 * @Description: 分页查询，返回列表
	 * new PageBounds();//默认构造函数不提供分页，返回ArrayList  
	 * new PageBounds(int limit);//取TOPN操作，返回ArrayList  
     * new PageBounds(Order... order);//只排序不分页，返回ArrayList  
     * new PageBounds(int page, int limit);//默认分页，返回PageList  
     * new PageBounds(int page, int limit, Order... order);//分页加排序，返回PageList  
     * //使用containsTotalCount来决定查不查询totalCount，即返回ArrayList还是PageList
     * new PageBounds(int page, int limit, List<Order> orders,boolean containsTotalCount);  
     * 示例：int page = 1; //页号  
	 *		int pageSize = 20; //每页数据条数  
	 *		String sortString = "age.asc,gender.desc";//如果你想排序的话逗号分隔可以排序多列  
	 *		PageBounds pageBounds = newPageBounds(page, pageSize , Order.formString(sortString)); 
	 *		PageList pageList = (PageList)getPageList(pageBounds,maps);
	 * @param: Map<String,Object> 参数Map
	 * @param: PageBounds 分页对象
	 * @return: List<T>
	 * @throws:
	 * @Author: chenshuai
	 * @Create: Date: 2015年1月30日 下午3:06:31
	 */
	public List<T> getPageList(PageBounds pageBounds,Map<String,Object> params) throws Exception ;
	/**
	 * @Description: 通过参数查询，返回列表List
	 * @param: Map<String,Object>
	 * @return: List<T>
	 * @throws:
	 * @Author: chenshuai
	 * @Create: Date: 2015年1月29日 下午7:31:50
	 */
	public List<T> find(Map<String,Object> params) throws Exception ;
	/**
	 * @Description: 通过主键或唯一约束获取对象
	 * @param:	String
	 * @return: T
	 * @throws:
	 * @Author: chenshuai
	 * @Create: Date: 2015年1月29日 下午7:32:26
	 */
	public T getById(String id) throws Exception ;
	/**
	 * @Description: 保存对象
	 * @param: T
	 * @return: void
	 * @throws:
	 * @Author: chenshuai
	 * @Create: Date: 2015年1月29日 下午7:34:04
	 */
	public void save(T t) throws Exception ;
	/**
	 * @Description: 更新对象
	 * @param: T
	 * @return: void
	 * @throws:
	 * @Author: chenshuai
	 * @Create: Date: 2015年1月29日 下午7:34:28
	 */
	public void update(T t) throws Exception ;
	/**
	 * @Description: 通过ID删除对象
	 * @param: String
	 * @return: void
	 * @throws:
	 * @Author: chenshuai
	 * @Create: Date: 2015年1月29日 下午7:34:48
	 */
	public void deleteById(String id) throws Exception ;
	/**
	 * @Description: 通过ID列表批量删除
	 * @param: List<String>
	 * @return: int
	 * @throws:
	 * @Author: chenshuai
	 * @Create: Date: 2015年1月29日 下午7:35:19
	 */
	public int delete(List<String> ids) throws Exception ;
	
}
