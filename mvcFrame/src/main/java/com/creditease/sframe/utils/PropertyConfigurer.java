/* 
 * Copyright (C) 2006-2013 普信恒业科技发展（北京）有限公司.
 * 本系统是商用软件,未经授权擅自复制或传播本程序的部分或全部将是非法的.
============================================================
 * FileName: PropertyConfig.java 
 * Created: [May 31, 2013 11:17:14 AM] by Administrator 
 * $Id$
 * $Revision$
 * $Author$
 * $Date$
============================================================ 
 * ProjectName: mspl_dev 
 * Description: 
==========================================================*/

package com.creditease.sframe.utils;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

/** 
 * Description: 属性文件配置类，用与加载系统中使用的.properties文件，覆盖了spring的
 * PropertyPlaceholderConfigurer类，主要目的就是为了可以在代码中直接调用getContextProperty
 * 获取属于文件中对应的配置信息
 * @author chens
 * @version 1.0
 * <pre>
 * Modification History: 
 * Date         Author      Version     Description 
------------------------------------------------------------------
 * May 31, 2013    Administrator       1.0        1.0 Version 
 * </pre>
 */

public class PropertyConfigurer extends PropertyPlaceholderConfigurer {

private static Map<String, Object> ctxPropertiesMap;
	
	@Override
	protected void processProperties(
			ConfigurableListableBeanFactory beanFactoryToProcess,
			Properties props) throws BeansException {
		// TODO Auto-generated method stub
		super.processProperties(beanFactoryToProcess, props);
		//load properties to ctxPropertiesMap  
		ctxPropertiesMap = new HashMap<String, Object>();
		for (Object key : props.keySet()) {
			String keyStr = key.toString();
			String value = props.getProperty(keyStr);
			ctxPropertiesMap.put(keyStr, value);
			
		}
	}
	/**
	 * static method for accessing context properties  
	 * @param name
	 * @return
	 */
	public static Object getContextProperty(String name) {
		return ctxPropertiesMap.get(name);
	}
}
