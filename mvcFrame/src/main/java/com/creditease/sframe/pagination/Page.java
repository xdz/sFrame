
package com.creditease.sframe.pagination;


/**
 * 分页类
 * @author 
 * @version 
 * @param <T>
 */
public class Page<T> {
	
	private int pageNo = 1; // 当前页码
	private int pageSize = 10; // 页面大小，设置为“-1”表示不进行分页（分页无效）
	private String sortString = ""; // 标准查询有效， 实例： updatedate desc, name asc
	
	//private int offset=pageSize * (pageNo - 1);//分页起始位置
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public String getSortString() {
		return sortString;
	}
	public void setSortString(String sortString) {
		this.sortString = sortString;
	}
}
