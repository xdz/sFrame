/* 
 * Copyright (C) 2006-2013 普信恒业科技发展（北京）有限公司.
 * 本系统是商用软件,未经授权擅自复制或传播本程序的部分或全部将是非法的.
============================================================
 * FileName: CheckedException.java 
 * Created: [May 31, 2013 11:23:12 AM] by chenshuai 
 * $Id$
 * $Revision$
 * $Author$
 * $Date$
============================================================ 
 * ProjectName: baseUnit 
 * Description: 
==========================================================*/

package com.creditease.sframe.exception;

import java.util.ArrayList;
import java.util.List;

/** 
 * Description: 应用级异常（业务人员误操作产生的异常，如用户不存在、密码错误等等）的基类，是一个受控异常基类。
 * @author chens
 * @version 1.0
 * <pre>
 * Modification History: 
 * Date         Author      Version     Description 
------------------------------------------------------------------
 * May 31, 2013    Administrator       1.0        1.0 Version 
 * </pre>
 */

public class CheckedException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
     * 错误Key，用于唯一标识错误类型。
     */
    private String exceptionKey = null;

    /**
     * 传递给变量的错误值
     */
    private Object[] exceptionValues = null;


    public CheckedException() {
        super();
    }

    public CheckedException(String key) {
        super();
        this.exceptionKey = key;
        this.exceptionValues = null;
        //转成明文错误信息
    }

    public CheckedException(String key, Object[] args) {
        super();
        this.exceptionKey = key;
        this.exceptionValues = args;
        //转成明文错误信息
    }

    public CheckedException(Throwable rootCause, String key, Object[] args) {
        super(rootCause);
        this.exceptionKey = key;
        this.exceptionValues = args;
        //转成明文错误信息
    }

    public Object[] getExceptionValues() {
        return exceptionValues;
    }

    /**
     * 获取异常Key
     *
     * @return 异常Key
     */
    public String getExceptionKey() {
        return exceptionKey;
    }

    /**
     * 取得转成明文的错误信息
     *
     * @return String 返回转成明文的错误信息
     */
    public String getMessage() {
    	return super.getMessage();
    }

    public String getMessagesString() {
        String[] msgs = this.getMessages();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < msgs.length; i++) {
            sb.append(msgs[i] + "\n");
        }
        return sb.toString();
    }

    public String[] getMessages() {
        Throwable[] throwables = this.getThrowables();
        String[] msgs = new String[throwables.length];
        for (int i = 0; i < throwables.length; i++) {
            msgs[i] = throwables[i].getMessage();
        }
        return msgs;
    }

    public Throwable[] getThrowables() {
        List list = new ArrayList();
        Throwable throwable = this;
        while (throwable != null) {
            list.add(throwable);
            throwable = throwable.getCause();
        }
        return (Throwable[]) list.toArray(new Throwable[list.size()]);
    }
}
