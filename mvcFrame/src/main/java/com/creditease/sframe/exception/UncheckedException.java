/* 
 * Copyright (C) 2006-2013 普信恒业科技发展（北京）有限公司.
 * 本系统是商用软件,未经授权擅自复制或传播本程序的部分或全部将是非法的.
============================================================
 * FileName: UncheckedException.java 
 * Created: [May 31, 2013 10:58:43 AM] by chenshuai 
 * $Id$
 * $Revision$
 * $Author$
 * $Date$
============================================================ 
 * ProjectName: baseUnit 
 * Description: 
==========================================================*/

package com.creditease.sframe.exception;

import com.creditease.sframe.utils.PropertyConfigurer;

/** 
 * Description: 系统级异常的基类，是一个不受控异常基类。
 * 				可不用捕捉或传throws方式传递下去。
 * @author chens
 * @version 1.0
 * <pre>
 * Modification History: 
 * Date         Author      Version     Description 
------------------------------------------------------------------
 * May 31, 2013    Administrator       1.0        1.0 Version 
 * </pre>
 */

public class UncheckedException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	/**
	 * 错误Key，用于唯一标识错误类型。
	 */
	private String errorCode = null;
	/**
	 * 传递给变量的错误值
	 */
	private Object[] errorParam = null;

	public UncheckedException(String errorCode) {
		this.errorCode = errorCode;
	}

	public UncheckedException(String errorCode, Object[] errorParam) {
		this.errorCode = errorCode;
		this.errorParam = errorParam;
	}

	public UncheckedException(String errorCode, Object[] errorParam, Throwable t) {
		super(t);
		this.errorCode = errorCode;
		this.errorParam = errorParam;
	}

	public UncheckedException(String message, Throwable t) {
		super(message, t);
	}

	public String getErrorCode() {
		return this.errorCode;
	}

	public Object[] getErrorParam() {
		return this.errorParam;
	}

	public String getMessage() {
		if (errorCode != null && !errorCode.trim().equals("")) {
			return (String) PropertyConfigurer.getContextProperty(errorCode);
		}
		return super.getMessage();
	}
}
