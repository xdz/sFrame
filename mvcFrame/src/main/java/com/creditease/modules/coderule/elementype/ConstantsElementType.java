/* 
 * Copyright (C) 2006-2013 普信恒业科技发展（北京）有限公司.
 * 本系统是商用软件,未经授权擅自复制或传播本程序的部分或全部将是非法的.
============================================================
 * FileName: ConstantsElementType.java 
 * Created: [Jun 6, 2013 11:16:18 AM] by Administrator 
 * $Id$
 * $Revision$
 * $Author$
 * $Date$
============================================================ 
 * ProjectName: mspl_dev 
 * Description: 
==========================================================*/

package com.creditease.modules.coderule.elementype;

import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.creditease.sframe.exception.UncheckedException;

/** 
 * Description: 常量元素类型，因为是常量，所以直接取元素值+分隔符返回即可，不需要做其他处理
 * @author chens
 * @version 1.0
 * <pre>
 * Modification History: 
 * Date         Author      Version     Description 
------------------------------------------------------------------
 * Jun 6, 2013    Administrator       1.0        1.0 Version 
 * </pre>
 */
@Service("BET_001")
public class ConstantsElementType implements IElementType {

	/* (non-Javadoc)
	 * @see com.creditease.mspl.coderule.elementype.IElementType#getElementTypeString(java.util.Map)
	 */
	public String getElementTypeString(Map<String, String> value_separator) {
		// TODO Auto-generated method stub
		String valueString = "";
		if(value_separator != null){
			String value = value_separator.get("ELEMENT_VALUE");
			String separator = value_separator.get("ELEMENT_SEPARATOR");
			if(StringUtils.hasText(value) && StringUtils.hasText(separator)){
				valueString += value;
				valueString += separator;
			}else{
				throw new UncheckedException("代码规则元素信息为空，无法完成代码转换！");
			}
		}else{
			throw new UncheckedException("代码规则元素信息为空，无法完成代码转换！");
		}
		return valueString;
	}

}
