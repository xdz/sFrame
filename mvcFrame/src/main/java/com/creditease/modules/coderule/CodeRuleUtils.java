/* 
 * Copyright (C) 2006-2013 普信恒业科技发展（北京）有限公司.
 * 本系统是商用软件,未经授权擅自复制或传播本程序的部分或全部将是非法的.
============================================================
 * FileName: CodeRuleUtils.java 
 * Created: [Jun 6, 2013 1:41:16 PM] by Administrator 
 * $Id$
 * $Revision$
 * $Author$
 * $Date$
============================================================ 
 * ProjectName: mspl_dev 
 * Description: 
==========================================================*/

package com.creditease.modules.coderule;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.util.StringUtils;

import com.creditease.sframe.exception.UncheckedException;

/** 
 * Description: 代码规则工具类
 * @author chens
 * @version 1.0
 * <pre>
 * Modification History: 
 * Date         Author      Version     Description 
------------------------------------------------------------------
 * Jun 6, 2013    Administrator       1.0        1.0 Version 
 * </pre>
 */

public class CodeRuleUtils {

	public final static String DATE_FORMAT = "yyyy-MM-dd";
	/**
	 * Description: 获取当前日期字符串
	 * @param
	 * @return String
	 * @throws
	 * @Author Administrator
	 * Create Date: Jun 6, 2013 1:41:50 PM
	 */
	public static String getCurrentDateToString(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(new Date());
	}
	/**
	 * Description: 获取当前日期年
	 * @param
	 * @return String
	 * @throws
	 * @Author Administrator
	 * Create Date: Jun 6, 2013 1:42:26 PM
	 */
	public static String getCurrentYear(){
		return getCurrentDateToString().substring(0,4);
	}
	/**
	 * Description: 获取当前日期月份
	 * @param
	 * @return String
	 * @throws
	 * @Author Administrator
	 * Create Date: Jun 6, 2013 1:43:04 PM
	 */
	public static String getCurrentMonth(){
		return getCurrentDateToString().substring(5,7);
	}
	/** 获取当前日期日
	 * Description: 
	 * @param
	 * @return String
	 * @throws
	 * @Author Administrator
	 * Create Date: Jun 6, 2013 1:43:36 PM
	 */
	public static String getCurrentDay(){
		return getCurrentDateToString().substring(8,10);
	}
	/**
	 * Description: 按位数补0，例： digits=5 value=1 则返回值为：00001
	 * @param digits 位数
	 * @param value  当前值
	 * @return String
	 * @throws
	 * @Author Administrator
	 * Create Date: Jun 6, 2013 1:51:04 PM
	 */
	public static String supplementDigits(String value,String digits){
		if(sequenceIndexOutOfBounds(value,digits)){
			throw new UncheckedException("流水号越界，检查更新周期数据是否存在，数据库数据错误！");
		}
		String zeroStr = "";
		int valueLength = value.length();
		int digitsInt = Integer.valueOf(digits);
		for(int i=0;i<(digitsInt - valueLength);i++){
			zeroStr += "0";
		}
		return zeroStr + value;
	}
	/**
	 * Description: 流水号越界判断
	 * @param value 当前值
	 * @param digits 位数
	 * @return boolean
	 * @throws
	 * @Author chens
	 * Create Date: Jun 6, 2013 1:48:43 PM
	 */
	private static boolean sequenceIndexOutOfBounds(String value,String digits){
		if(StringUtils.hasText(value) && StringUtils.hasText(digits)){
			StringBuffer maxNumber = new StringBuffer();
			for(int i=0;i<Integer.valueOf(digits);i++){
				maxNumber.append("9");
			}
			if(Integer.valueOf(value) > Integer.valueOf(maxNumber.toString())){
				return true;
			}
		}else{
			throw new UncheckedException("无法得到下一个序号、或者流水号位数不存在，数据库数据错误！");
		}
		return false;
	}
	/**
	 * Description: 把一个Date日期按照yyyy-MM-dd的固定格式转换为String
	 * @param date 日期类型
	 * @return String
	 * @throws
	 * @Author chens
	 * Create Date: Jun 6, 2013 2:14:44 PM
	 */
	public static String dateToString(Date date, String format){
		if(!StringUtils.hasText(format)){
			format = DATE_FORMAT;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}
	/**
	 * Description: 根据某一个特定日期(Date)返回该日期的年份
	 * @param
	 * @return String
	 * @throws
	 * @Author Administrator
	 * Create Date: Jun 6, 2013 2:15:43 PM
	 */
	public static String dateYearToString(Date date){
		return dateToString(date,null).substring(0,4);
	}
	/**
	 * Description: 根据某一个特定日期(Date)返回该日期的月份
	 * @param
	 * @return String
	 * @throws
	 * @Author Administrator
	 * Create Date: Jun 6, 2013 2:16:06 PM
	 */
	public static String dateMonthToString(Date date){
		return dateToString(date,null).substring(5,7);
	}
	/**
	 * Description: 根据某一个特定日期(Date)返回该日期的日份
	 * @param
	 * @return String
	 * @throws
	 * @Author Administrator
	 * Create Date: Jun 6, 2013 2:16:27 PM
	 */
	public static String dateDayToString(Date date){
		return dateToString(date,null).substring(8,10);
	}
}
