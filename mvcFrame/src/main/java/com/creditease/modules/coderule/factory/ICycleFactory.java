/* 
 * Copyright (C) 2006-2013 普信恒业科技发展（北京）有限公司.
 * 本系统是商用软件,未经授权擅自复制或传播本程序的部分或全部将是非法的.
============================================================
 * FileName: ICycleFactory.java 
 * Created: [Jun 6, 2013 2:51:55 PM] by Administrator 
 * $Id$
 * $Revision$
 * $Author$
 * $Date$
============================================================ 
 * ProjectName: mspl_dev 
 * Description: 
==========================================================*/

package com.creditease.modules.coderule.factory;

import com.creditease.modules.coderule.cycle.ICycleElement;

/** 
 * Description: 周期管理的工厂，根据序列周期类型选择不同的周期处理bean
 * @author chens
 * @version 1.0
 * <pre>
 * Modification History: 
 * Date         Author      Version     Description 
------------------------------------------------------------------
 * Jun 6, 2013    Administrator       1.0        1.0 Version 
 * </pre>
 */
public interface ICycleFactory {

	public final static String BEAN_ID = "cycleFactory";
	/**
	 * Description: 根据一个beanId在spring中得到一个实现了ICycleElement接口的实体对象
	 * @param
	 * @return ICycleElement
	 * @throws
	 * @Author Administrator
	 * Create Date: Jun 6, 2013 2:53:13 PM
	 */
	public ICycleElement creatCycleElement(String beanId);
}
