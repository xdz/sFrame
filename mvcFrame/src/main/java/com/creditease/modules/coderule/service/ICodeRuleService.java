/* 
 * Copyright (C) 2006-2013 普信恒业科技发展（北京）有限公司.
 * 本系统是商用软件,未经授权擅自复制或传播本程序的部分或全部将是非法的.
============================================================
 * FileName: ICodeRuleService.java 
 * Created: [Jun 5, 2013 5:02:46 PM] by Administrator 
 * $Id$
 * $Revision$
 * $Author$
 * $Date$
============================================================ 
 * ProjectName: mspl_dev 
 * Description: 
==========================================================*/

package com.creditease.modules.coderule.service;

/** 
 * Description: 代码规则服务类，用于生成各种业务代码
 * @author chens
 * @version 1.0
 * <pre>
 * Modification History: 
 * Date         Author      Version     Description 
------------------------------------------------------------------
 * Jun 5, 2013    Administrator       1.0        1.0 Version 
 * </pre>
 */
public interface ICodeRuleService {

	/**
	 * Description:根据业务代码生成一个业务编码 
	 * 根据业务代码获取代码规则要素列表，按要素顺序排序
	 * 循环规则要素进行处理
	 * 一、判断元素代码 BET_001 常量  那么取值就为： 元素值+分隔符
	 * 二、判断元素代码 BET_002 变量 YEAR、MONTH、DAY 然后根据变量代码获取相应的值 此时的值为：变量值+分隔符
	 * 三、判断元素代码 BET_003 流水号位数 
	 * 		a，根据SERIAL_ID获取对应的序号属性，根据循环周期判断是否重新开始，并获取值，结合流水号位数，不足以0添补
	 * 		b，获取值=将当前值+步长
	 * @param
	 * @return String
	 * @throws
	 * @Author Administrator
	 * Create Date: Jun 5, 2013 5:04:10 PM
	 */
	public String getBizCode(String bizCode);
}
