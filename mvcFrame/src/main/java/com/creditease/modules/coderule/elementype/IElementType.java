/* 
 * Copyright (C) 2006-2013 普信恒业科技发展（北京）有限公司.
 * 本系统是商用软件,未经授权擅自复制或传播本程序的部分或全部将是非法的.
============================================================
 * FileName: IElementType.java 
 * Created: [Jun 6, 2013 11:05:49 AM] by Administrator 
 * $Id$
 * $Revision$
 * $Author$
 * $Date$
============================================================ 
 * ProjectName: mspl_dev 
 * Description: 
==========================================================*/

package com.creditease.modules.coderule.elementype;

import java.util.Map;

/** 
 * Description: 代码规则元素类型接口，代码规则中元素类型大致分为三种，常量、变量、流水号
 * 				我们会为每一个元素类型编写一个实体类，以便处理每个类型的元素值的获取
 * @author chens
 * @version 1.0
 * <pre>
 * Modification History: 
 * Date         Author      Version     Description 
------------------------------------------------------------------
 * Jun 6, 2013    Administrator       1.0        1.0 Version 
 * </pre>
 */

public interface IElementType {

	/**
	 * Description: 根据map对象返回元素处理后的字符串值
	 * @param value_separator 一个Map对象，存放的信息应包含两个：（key值对应数据表里的字段）
	 * 			<"element_value","xxxxx">		元素值
	 * 			<"element_separator","xxxxx">	元素分隔符
	 * @return String
	 * @throws
	 * @Author Administrator
	 * Create Date: Jun 6, 2013 11:06:29 AM
	 */
	public String getElementTypeString(Map<String,String> value_separator);
}
