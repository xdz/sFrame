/* 
 * Copyright (C) 2006-2013 普信恒业科技发展（北京）有限公司.
 * 本系统是商用软件,未经授权擅自复制或传播本程序的部分或全部将是非法的.
============================================================
 * FileName: IElementTypeFactory.java 
 * Created: [Jun 6, 2013 3:25:56 PM] by Administrator 
 * $Id$
 * $Revision$
 * $Author$
 * $Date$
============================================================ 
 * ProjectName: mspl_dev 
 * Description: 
==========================================================*/

package com.creditease.modules.coderule.factory;

import com.creditease.modules.coderule.elementype.IElementType;

/** 
 * Description: 元素类型生产工厂
 * @author chens
 * @version 1.0
 * <pre>
 * Modification History: 
 * Date         Author      Version     Description 
------------------------------------------------------------------
 * Jun 6, 2013    Administrator       1.0        1.0 Version 
 * </pre>
 */

public interface IElementTypeFactory {

	public final static String BEAN_ID = "elementTypeFactory";
	/**
	 * Description: 根据bendID获取元素类型
	 * @param
	 * @return IElementType
	 * @throws
	 * @Author Administrator
	 * Create Date: Jun 6, 2013 3:26:36 PM
	 */
	public IElementType creatElementType(String beanId);
}
