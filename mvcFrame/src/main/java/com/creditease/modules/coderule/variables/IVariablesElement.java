/* 
 * Copyright (C) 2006-2013 普信恒业科技发展（北京）有限公司.
 * 本系统是商用软件,未经授权擅自复制或传播本程序的部分或全部将是非法的.
============================================================
 * FileName: IVariablesElement.java 
 * Created: [Jun 6, 2013 1:39:23 PM] by Administrator 
 * $Id$
 * $Revision$
 * $Author$
 * $Date$
============================================================ 
 * ProjectName: mspl_dev 
 * Description: 
==========================================================*/

package com.creditease.modules.coderule.variables;

/** 
 * Description: 变量元素接口类
 * @author chens
 * @version 1.0
 * <pre>
 * Modification History: 
 * Date         Author      Version     Description 
------------------------------------------------------------------
 * Jun 6, 2013    Administrator       1.0        1.0 Version 
 * </pre>
 */

public interface IVariablesElement {

	/**
	 * Description: 处理变量类型的接口类
	 * @param
	 * @return String
	 * @throws
	 * @Author Administrator
	 * Create Date: Jun 6, 2013 1:39:56 PM
	 */
	public String getVariablesElementValue();
}
