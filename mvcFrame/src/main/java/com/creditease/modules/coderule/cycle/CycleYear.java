/* 
 * Copyright (C) 2006-2013 普信恒业科技发展（北京）有限公司.
 * 本系统是商用软件,未经授权擅自复制或传播本程序的部分或全部将是非法的.
============================================================
 * FileName: CycleYear.java 
 * Created: [Jun 6, 2013 3:19:42 PM] by Administrator 
 * $Id$
 * $Revision$
 * $Author$
 * $Date$
============================================================ 
 * ProjectName: mspl_dev 
 * Description: 
==========================================================*/

package com.creditease.modules.coderule.cycle;

import java.util.Date;

import org.springframework.stereotype.Service;

import com.creditease.modules.coderule.CodeRuleUtils;

/** 
 * Description: 年循环处理类，该类特指一年循环一次
 * @author chens
 * @version 1.0
 * <pre>
 * Modification History: 
 * Date         Author      Version     Description 
------------------------------------------------------------------
 * Jun 6, 2013    Administrator       1.0        1.0 Version 
 * </pre>
 */
@Service("YEAR_CYCLE")
public class CycleYear implements ICycleElement {

	/* (non-Javadoc)
	 * @see com.creditease.mspl.coderule.cycle.ICycleElement#isWarrantyCycle(java.util.Date)
	 */
	public boolean isWarrantyCycle(Date updateTime) {
		// TODO Auto-generated method stub
		String format = "yyyy";
		//当前日期字符串
		String currentDate = CodeRuleUtils.dateToString(new Date(),format);
		//比较日期字符串
		String compareDate = CodeRuleUtils.dateToString(updateTime,format);
		//年、月、日都与当前日期相等，则认为循环可以继续，不需要进行序号重置
		if(currentDate.equals(compareDate)){
			return true;
		}else{
			return false;
		}
	}

}
