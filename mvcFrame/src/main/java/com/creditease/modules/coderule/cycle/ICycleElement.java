/* 
 * Copyright (C) 2006-2013 普信恒业科技发展（北京）有限公司.
 * 本系统是商用软件,未经授权擅自复制或传播本程序的部分或全部将是非法的.
============================================================
 * FileName: ICycleElement.java 
 * Created: [Jun 6, 2013 2:54:15 PM] by Administrator 
 * $Id$
 * $Revision$
 * $Author$
 * $Date$
============================================================ 
 * ProjectName: mspl_dev 
 * Description: 
==========================================================*/

package com.creditease.modules.coderule.cycle;

import java.util.Date;

/** 
 * Description: 流水号更新周期接口类
 * @author chens
 * @version 1.0
 * <pre>
 * Modification History: 
 * Date         Author      Version     Description 
------------------------------------------------------------------
 * Jun 6, 2013    Administrator       1.0        1.0 Version 
 * </pre>
 */

public interface ICycleElement {

	/**
	 * Description: 判断某日期是否在循环周期之内
	 * @param
	 * @return boolean true 不需要重置流水号|false 需要重置流水号
	 * @throws
	 * @Author Administrator
	 * Create Date: Jun 6, 2013 2:54:45 PM
	 */
	public boolean isWarrantyCycle(Date updateTime);
}
