/* 
 * Copyright (C) 2006-2013 普信恒业科技发展（北京）有限公司.
 * 本系统是商用软件,未经授权擅自复制或传播本程序的部分或全部将是非法的.
============================================================
 * FileName: VariablesElementType.java 
 * Created: [Jun 6, 2013 11:44:16 AM] by Administrator 
 * $Id$
 * $Revision$
 * $Author$
 * $Date$
============================================================ 
 * ProjectName: mspl_dev 
 * Description: 
==========================================================*/

package com.creditease.modules.coderule.elementype;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.creditease.modules.coderule.factory.IVariablesFactory;
import com.creditease.modules.coderule.variables.IVariablesElement;
import com.creditease.sframe.exception.UncheckedException;

/** 
 * Description: 变量元素类型，需要根据元素值（此时的每一个元素值就对应一个Spring的一个beanID）获取相应的变量值
 * @author chens
 * @version 1.0
 * <pre>
 * Modification History: 
 * Date         Author      Version     Description 
------------------------------------------------------------------
 * Jun 6, 2013    Administrator       1.0        1.0 Version 
 * </pre>
 */
@Service("BET_002")
public class VariablesElementType implements IElementType {

	//变量工厂接口，该接口的实现工厂类在spring中配置
	@Resource(name="variablesFactory")
	private IVariablesFactory variablesFactory;
	/* (non-Javadoc)
	 * @see com.creditease.mspl.coderule.elementype.IElementType#getElementTypeString(java.util.Map)
	 */
	public String getElementTypeString(Map<String, String> value_separator) {
		// TODO Auto-generated method stub
		String valueString = "";
		if(value_separator != null){
			//此时的元素值对应着spring的一个bean
			String beanId = value_separator.get("ELEMENT_VALUE");
			String separator = value_separator.get("ELEMENT_SEPARATOR");
			
			IVariablesElement variablesElement = variablesFactory.creatVariablesElement(beanId);
			valueString += variablesElement.getVariablesElementValue();
			//获取分隔符、分隔符如果为null就不要追加了
			if(StringUtils.hasText(separator)){
				valueString += separator;
			}
		}else{
			throw new UncheckedException("MSPL_CR_0001_01");
		}
		return valueString;
	}
}
