/* 
 * Copyright (C) 2006-2013 普信恒业科技发展（北京）有限公司.
 * 本系统是商用软件,未经授权擅自复制或传播本程序的部分或全部将是非法的.
============================================================
 * FileName: VariableMonth.java 
 * Created: [Jun 6, 2013 2:40:56 PM] by Administrator 
 * $Id$
 * $Revision$
 * $Author$
 * $Date$
============================================================ 
 * ProjectName: mspl_dev 
 * Description: 
==========================================================*/

package com.creditease.modules.coderule.variables;

import org.springframework.stereotype.Service;

import com.creditease.modules.coderule.CodeRuleUtils;

/** 
 * Description: 月变量处理类
 * @author chens
 * @version 1.0
 * <pre>
 * Modification History: 
 * Date         Author      Version     Description 
------------------------------------------------------------------
 * Jun 6, 2013    Administrator       1.0        1.0 Version 
 * </pre>
 */
@Service("MONTH_VAR")
public class VariableMonth implements IVariablesElement {

	/* (non-Javadoc)
	 * @see com.creditease.mspl.coderule.variables.IVariablesElement#getVariablesElementValue()
	 */
	public String getVariablesElementValue() {
		// TODO Auto-generated method stub
		return CodeRuleUtils.getCurrentMonth();
	}

}
