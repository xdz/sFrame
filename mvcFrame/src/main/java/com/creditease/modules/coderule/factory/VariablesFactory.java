/* 
 * Copyright (C) 2006-2013 普信恒业科技发展（北京）有限公司.
 * 本系统是商用软件,未经授权擅自复制或传播本程序的部分或全部将是非法的.
============================================================
 * FileName: VariablesFactory.java 
 * Created: [Jun 6, 2013 2:44:14 PM] by Administrator 
 * $Id$
 * $Revision$
 * $Author$
 * $Date$
============================================================ 
 * ProjectName: mspl_dev 
 * Description: 
==========================================================*/

package com.creditease.modules.coderule.factory;

import org.springframework.stereotype.Service;

import com.creditease.modules.coderule.variables.IVariablesElement;
import com.creditease.sframe.utils.SpringContextHolder;

/** 
 * Description: 
 * @author chens
 * @version 1.0
 * <pre>
 * Modification History: 
 * Date         Author      Version     Description 
------------------------------------------------------------------
 * Jun 6, 2013    Administrator       1.0        1.0 Version 
 * </pre>
 */
@Service
public class VariablesFactory implements IVariablesFactory {

	/* (non-Javadoc)
	 * @see com.creditease.mspl.coderule.factory.IVariablesFactory#creatVariablesElement(java.lang.String)
	 */
	public IVariablesElement creatVariablesElement(String beanId) {
		// TODO Auto-generated method stub
		return (IVariablesElement) SpringContextHolder.getBean(beanId);
	}

}
