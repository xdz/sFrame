/* 
 * Copyright (C) 2006-2013 普信恒业科技发展（北京）有限公司.
 * 本系统是商用软件,未经授权擅自复制或传播本程序的部分或全部将是非法的.
============================================================
 * FileName: CycleFactory.java 
 * Created: [Jun 6, 2013 3:21:45 PM] by Administrator 
 * $Id$
 * $Revision$
 * $Author$
 * $Date$
============================================================ 
 * ProjectName: mspl_dev 
 * Description: 
==========================================================*/

package com.creditease.modules.coderule.factory;

import org.springframework.stereotype.Service;

import com.creditease.modules.coderule.cycle.ICycleElement;
import com.creditease.sframe.utils.SpringContextHolder;

/** 
 * Description: 循环处理工厂实现类
 * @author chens
 * @version 1.0
 * <pre>
 * Modification History: 
 * Date         Author      Version     Description 
------------------------------------------------------------------
 * Jun 6, 2013    Administrator       1.0        1.0 Version 
 * </pre>
 */
@Service("cycleFactory")
public class CycleFactory implements ICycleFactory {

	/* (non-Javadoc)
	 * @see com.creditease.mspl.coderule.factory.ICycleFactory#creatCycleElement(java.lang.String)
	 */
	public ICycleElement creatCycleElement(String beanId) {
		// TODO Auto-generated method stub
		return (ICycleElement) SpringContextHolder.getBean(beanId);
	}

}
