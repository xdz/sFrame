/* 
 * Copyright (C) 2006-2013 普信恒业科技发展（北京）有限公司.
 * 本系统是商用软件,未经授权擅自复制或传播本程序的部分或全部将是非法的.
============================================================
 * FileName: CodeRuleUtils.java 
 * Created: [Jun 6, 2013 1:41:16 PM] by Administrator 
 * $Id$
 * $Revision$
 * $Author$
 * $Date$
============================================================ 
 * ProjectName: mspl_dev 
 * Description: 
==========================================================*/
package com.creditease.modules.coderule.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.creditease.sframe.persistence.IBaseDao;
import com.creditease.sframe.persistence.annotation.MyBatisDao;

/**
 * 
 * @Description：代码规则Dao
 * @author：
 * @CreateDate：
 * @Company:
 * @version 
 */

@MyBatisDao
public interface CodeRuleDao extends IBaseDao<HashMap>{
	/**
	 * 查询序列号
	 * @param map
	 * @return
	 */
	public Map querySerials(Map map);
	/**
	 * 获取代码规则元素
	 * @param map
	 * @return
	 */
	public List queryCoderuleElements(Map map);
	/**
	 * 更新序列号
	 * @param map
	 */
	public void updateSerials(Map map);
}
