/* 
 * Copyright (C) 2006-2013 普信恒业科技发展（北京）有限公司.
 * 本系统是商用软件,未经授权擅自复制或传播本程序的部分或全部将是非法的.
============================================================
 * FileName: CodeRuleServiceImpl.java 
 * Created: [Jun 5, 2013 5:06:47 PM] by Administrator 
 * $Id$
 * $Revision$
 * $Author$
 * $Date$
============================================================ 
 * ProjectName: mspl_dev 
 * Description: 
==========================================================*/

package com.creditease.modules.coderule.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.creditease.modules.coderule.dao.CodeRuleDao;
import com.creditease.modules.coderule.elementype.IElementType;
import com.creditease.modules.coderule.factory.IElementTypeFactory;
import com.creditease.sframe.exception.UncheckedException;

/** 
 * Description: 代码规则服务实现类
 * @author chens
 * @version 1.0
 * <pre>
 * Modification History: 
 * Date         Author      Version     Description 
------------------------------------------------------------------
 * Jun 5, 2013    Administrator       1.0        1.0 Version 
 * </pre>
 */
@Service
public class CodeRuleServiceImpl implements ICodeRuleService {
	@Autowired
	private CodeRuleDao codeRuleDao;
	@Autowired
	private IElementTypeFactory elementTypeFactory;
	
	/**
	 * Description: 根据bizCode得到一个业务编码
	 * @param bizCode业务代码
	 * @return String
	 * @throws
	 * @Author Administrator
	 * Create Date: Jun 6, 2013 5:30:49 PM
	 */
	@SuppressWarnings("unchecked")
	public String getBizCode(String bizCode) {
		String result = "";
		List list = getCoderuleElements(bizCode);
		if(list != null && list.size() > 0 ){
			for(int i=0;i<list.size();i++){
				Map mapField  = (HashMap)list.get(i);
				String eleType = (String)mapField.get("ELEMENT_TYPE");
				IElementType elementType = elementTypeFactory.creatElementType(eleType);
				result += elementType.getElementTypeString(mapField);
			}
		}else{
			throw new UncheckedException("MSPL_CR_0001_01");
		}
		return result;
	}

	/**
	 * Description: 根据bizCode得到一个业务编码
	 * @param bizCode业务代码
	 * @return List
	 * @throws
	 * @Author Administrator
	 * Create Date: Jun 6, 2013 5:30:49 PM
	 */
	private List getCoderuleElements(String bizCode){
		Map map = new HashMap();
		map.put("biz_code", bizCode);
		return codeRuleDao.queryCoderuleElements(map);
	}
	
}
